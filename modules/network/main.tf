resource "google_compute_network" "vpc_network" {
  name    = "${var.namespace}-vpc"
  project = var.project
}