variable "project" {
  description = "The project ID where all resources will be launched."
  type        = string
}

variable "namespace" {
  description = "Used to group resources"
  type        = string
}

variable "region" {
  description = "GCP region"
  type        = string
  default     = "us-central1"
}