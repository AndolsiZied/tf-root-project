resource "google_service_account" "service_account" {
  account_id   = "tf-service-account-runner"
  display_name = "TF Service Account Runner"
  project      = var.project_id
}

resource "time_rotating" "key_rotation" {
  rotation_days = 30
}

resource "google_service_account_key" "service_account_key" {
  service_account_id = google_service_account.service_account.name

  keepers = {
    rotation_time = time_rotating.key_rotation.rotation_rfc3339
  }
}

resource "google_organization_iam_binding" "service_account_project_role" {
  org_id = var.org_id
  role   = "roles/resourcemanager.projectCreator"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_organization_iam_binding" "service_account_billing_role" {
  org_id = var.org_id
  role   = "roles/billing.user"

  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "service_account_storage_role" {
  project = var.project_id
  role    = "roles/storage.objectAdmin"

  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}