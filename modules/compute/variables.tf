variable "project" {
  description = "The project ID where all resources will be launched."
  type        = string
}

variable "region" {
  description = "GCP region."
  type        = string
  default     = "us-central1"
}

variable "zone" {
  description = "GCP zone"
  type        = string
  default     = "us-central1-b"
}

variable "namespace" {
  description = "Used to group resources."
  type        = string
}

variable "vpc" {
  description = "The VPC to be used."
  type        = any
}

variable "node_count" {
  description = "Cluster node count."
  type        = number
  default     = 3
}