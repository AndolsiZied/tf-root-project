variable "project_name" {
  description = "The name for the project."
  type        = string
}

variable "org_id" {
  description = "The organization ID."
  type        = string
}

variable "billing_account" {
  description = "The ID of the billing account to associate this project with."
  type        = string
}

variable "viewers" {
  description = "list of IAM-format members to set as project viewers."
  type        = list(string)
  default     = []
}

variable "owners" {
  description = "list of IAM-format members to set as project owners."
  type        = list(string)
  default     = []
}

variable "prefix" {
  description = "Prefix used to generate project id and name."
  type        = string
}

variable "activate_apis" {
  description = "List of api to activate."
  type        = list(string)
  default     = []
}

variable "namespace" {
  description = "Used to group resources."
  type        = string
}